import { Component, Input } from '@angular/core';
import { Post } from "app/bean/post";

@Component({
    selector: 'linhaDoTempo',
    templateUrl: './linhaDoTempo.component.html'
})
export class LinhaDoTempo  { 
    @Input() posts: Post[];
    curtir(index){
        this.posts[index].likes++;
        console.log("like");
    }
}
