import { Component, Input } from '@angular/core';

@Component({
    selector: 'header-fix',
    templateUrl: './header.component.html'
})
export class HeaderFix  { 
    @Input() usuario:string;
}