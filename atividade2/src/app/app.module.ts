import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderFix } from "app/header-fix/header.component";
import { LinhaDoTempo } from "app/linha-do-tempo/linhaDoTempo.component";

@NgModule({
  declarations: [
    AppComponent, 
    HeaderFix,
    LinhaDoTempo
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
