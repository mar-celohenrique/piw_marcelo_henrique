import { PostService } from './../service/post.service';
import { Component, Input } from '@angular/core';
import { Post } from "app/bean/post";

@Component({
    selector: 'linhaDoTempo',
    templateUrl: './linhaDoTempo.component.html'
})
export class LinhaDoTempo  { 
    @Input() posts: Post[];

    constructor(private postService:PostService) {
      this.posts = this.postService.listar();
    }

    like(post){
        console.log("like");
    }

    listar() {
      return this.posts;
    }

}
