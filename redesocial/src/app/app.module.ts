


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PostComponent } from './post-component/post.component';
import { PostInput } from './post-input/postInput.component';
import { LinhaDoTempo } from './linha-do-tempo/linhaDoTempo.component';
import { HeaderFix } from './header-fix/header.component';
import { routing } from './rotas/app.routing';
import { PostService } from './service/post.service';

@NgModule({
  declarations: [
    AppComponent, 
    HeaderFix,
    LinhaDoTempo,
    PostComponent,
    PostInput
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
