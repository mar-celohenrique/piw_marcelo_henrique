import { Post } from 'app/bean/post';
import { Injectable } from "@angular/core";

@Injectable()
export class PostService {

  private posts:Post[] = [];

  adicionar(post:Post) {
    this.posts.push(post);
  }

  exluir(post:Post) {
    let indice:number = this.posts.indexOf(post);
    this.posts.splice(indice, 1);
  }

  listar() {
    return this.posts;
  }
}