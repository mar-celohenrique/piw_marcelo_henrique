import { Component } from '@angular/core';
import { Post } from "app/bean/post";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  usuario: string = "Usuário";
}
