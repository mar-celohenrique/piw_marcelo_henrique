export class Post {
    static ID:number = 0;
    id:number;
    likes:number;
    constructor(public usuario:string, public texto:string) {
      this.id = Post.ID++;
      this.likes = 0;
    }
}