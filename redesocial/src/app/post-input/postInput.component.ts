import { Post } from 'app/bean/post';
import { Component} from '@angular/core';
import { PostService } from "app/service/post.service";

export class PostInput{

  usuario:string = "";
  texto:string = "";

  constructor(private postService:PostService) {}

  inserir() {
    this.postService.adicionar(new Post(this.usuario, this.texto));
  }

}