import { Post } from './../bean/post';

import { PostService } from './../service/post.service';
import { Component, Output, Input, EventEmitter } from "@angular/core";


@Component({
    selector: 'post',
    templateUrl: './post.component.html',
    providers: [PostService]
})

export class PostComponent {
    @Input() post:Post;
    @Output() evento:EventEmitter<Post>  = new EventEmitter<Post>();

    constructor(private postService:PostService) {}

    public like(post) {
        this.evento.emit(post);
        post.likes = post.likes + 1;
        console.log("curtida");
    }

    public excluir(post){
        console.log("excluir");
        this.postService.exluir(post);
    }
}