import { LinhaDoTempo } from './../linha-do-tempo/linhaDoTempo.component';
import { PostInput } from './../post-input/postInput.component';
import { Routes, RouterModule } from"@angular/router";

const APP_ROUTES: Routes = [
    {path:"", redirectTo:"/", pathMatch:"full"},
    {path:"/", component: LinhaDoTempo},
    {path:"/postar", component: PostInput}
]

export const routing = RouterModule.forRoot(APP_ROUTES);

