import { Atividade2Page } from './app.po';

describe('atividade2 App', function() {
  let page: Atividade2Page;

  beforeEach(() => {
    page = new Atividade2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
