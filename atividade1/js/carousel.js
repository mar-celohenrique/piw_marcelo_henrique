$(document).ready(function () {
    var carousel_width = 1024,
        slide_size = $(".carousel-slide div").length * carousel_width;

    $(".carousel").css("width", carousel_width);
    $(".carousel-slide").css("width", slide_size);

    $(".carousel-left").click(function () {
        move_carousel($(".carousel-shown").prev());
    });
    $(".carousel-right").click(function () {
        move_carousel($(".carousel-shown").next());
    });

    $(document).keyup(function (e) {
        if (e.which == 37) {
            if ($(".carousel-shown").prev().length) {
                move_carousel($(".carousel-shown").prev());
            }
        } else if (e.which == 39) {
            if ($(".carousel-shown").next().length) {
                move_carousel($(".carousel-shown").next());
            }
        }
    });

});

function move_carousel(to_div) {
    var to_multiplier = to_div.index(),
        to_position = 0 - (to_multiplier * 1024);

    $(".carousel-slide").animate({
        left: to_position
    }, 500, function () {
        $(".carousel-shown").removeClass("carousel-shown");
        to_div.addClass("carousel-shown");
        if (!$(".carousel-shown").prev().length) {
            $(".carousel-left").hide();
        } else {
            $(".carousel-left").show();
        }
        if (!$(".carousel-shown").next().length) {
            $(".carousel-right").hide();
        } else {
            $(".carousel-right").show();
        }
    });
}
